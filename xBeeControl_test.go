package xapiary

import (
	"fmt"
	"gitlab.com/DMDavis/xapiary/frames"
	"go.bug.st/serial"
	"log"
	"testing"
	"time"
)

func TestControl(t *testing.T) {

}

func TestCreateSend(t *testing.T) {
	ports, err := serial.GetPortsList()
	if err != nil {
		log.Fatal(err)
	}
	if len(ports) == 0 {
		fmt.Println("No serial ports found!")
	} else {
		for _, port := range ports {
			fmt.Printf("Found port: %v\n", port)
		}
	}
	port, err := serial.Open("/dev/tty.usbserial-D309S9C8", &serial.Mode{})
	if err != nil {
		t.Fatal(err)
	}
	mode := &serial.Mode{
		BaudRate: 9600,
		Parity:   serial.NoParity,
		DataBits: 8,
		StopBits: serial.OneStopBit,
	}
	if err := port.SetMode(mode); err != nil {
		t.Fatal(err)
	}
	fmt.Println("Port set to 9600 N81")

	xbee := NewXbee(port)

	outgoingFrame := frames.TransmitRequestFrame{
		Recipient: frames.XBee64Address(0x0013A20041EC468D),
		Payload:   []byte("This is a payload!"),
		FrameID:   0xBB,
	}

	err = xbee.Transmit(&outgoingFrame)
	if err != nil {
		t.Fatal(err)
	}

	time.Sleep(120 * time.Second)
}
