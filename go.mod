module gitlab.com/DMDavis/xapiary

go 1.17

require go.bug.st/serial v1.3.5

require (
	github.com/creack/goselect v0.1.2 // indirect
	gitlab.com/DMDavis/xapiary/frames v0.0.3
	golang.org/x/sys v0.0.0-20210823070655-63515b42dcdf // indirect
)
