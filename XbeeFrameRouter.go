package xapiary

import (
	"gitlab.com/DMDavis/xapiary/frames"
)

type FrameHandler func(frame frames.Frame)
