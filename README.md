# xApiary

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/DMDavis/xapiary.svg)](https://pkg.go.dev/gitlab.com/DMDavis/xapiary)

A library for managing your xBees! xApiary handles the direct reading and writing from 
to xBee radios in API mode 2. This library is still being built, so please know that
currently only reading and writing basic transmissions is supported.

Example usage (tested in MacOSX) :
```go
package main

import (
	"gitlab.com/DMDavis/xapiary"
	"gitlab.com/DMDavis/xapiary/frames"
	"go.bug.st/serial"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	//you will need to provide your 
	//own serial port name here  ▼▼▼
	port, err := serial.Open("/dev/tty.usbserial-D309T55O", &serial.Mode{})
	if err != nil {
		log.Fatal(err)
	}
	mode := &serial.Mode{
		BaudRate: 9600, // you can always set your xBees to a higher BAUD rate,
		// however, doing so must be done using XCTU !
		Parity:   serial.NoParity,
		DataBits: 8,
		StopBits: serial.OneStopBit,
	}

	if err := port.SetMode(mode); err != nil {
		log.Fatal(err)
	}
	defer port.Close()

	// Create XBee here! Always use NewXbee(serial port) to create XBee instances.
	xbee := xapiary.NewXbee(port)
	defer xbee.Close()
	// this is an anonymous implementation of a frameHandler, refer to the docs
	// for information on how to create handlers yourself.
	xbee.Handle(frames.RECEIVE_PACKET, func(frame frames.Frame) {
		//the type assertion here is guaranteed to pass by xApiary, so the 'ok'
		//flag is omitted. This should not be done in an error-resilient environment.
		rxFrame, _ := frame.(*frames.ReceivePacketFrame)
		log.Printf("%v\n", rxFrame)
	})

	//because the xBees are Asynchronous, you need to hold the main thread
	//open until the xbees are ready to be shut down.
	//
	//this standard os.Signal holding technique is quite useful - it will wait
	//until a ctrl-C char is sent, or the program is terminated in the IDE
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	<-c
}
```

xApiary has plans for support of the following features:
* Full frame ingestion for all supported API frames
* Full AT command support for reconfiguration of Radios in real time
* Error diagnosis and debugging for connection interference

If you would like to contribute, please build an issue and a pull-request with your 
suggested changes. Please follow any existing idioms where they are applicable.