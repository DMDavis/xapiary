package frames

import (
	"encoding/binary"
	"fmt"
)

func XbeeAddressFromBytes(raw16bit, raw64bit []byte) XBeeAddress {

	var addr64 uint64 = 0xFFFFFFFFFFFFFFFF
	var addr16 uint16 = 0xFFFE

	if len(raw64bit) == 8 {
		addr64 = binary.BigEndian.Uint64(raw64bit)
	}
	if len(raw16bit) == 2 {
		addr16 = binary.BigEndian.Uint16(raw16bit)
	}
	if addr16 != 0xFFFE && addr64 != 0xFFFFFFFFFFFFFFFF {
		return XBeeFullAddress{
			NetworkAddress: XBee16Address(addr16),
			MacAddress:     XBee64Address(addr64),
		}
	}
	if addr64 != 0xFFFFFFFFFFFFFFFF {
		return XBee64Address(addr64)
	} else if addr16 != 0xFFFE {
		return XBee16Address(addr16)
	}
	return nil
}

type XBeeAddress interface {
	Serialize() *[10]byte
}

type XBee16Address uint16

func (addr XBee16Address) Serialize() *[10]byte {
	return &[10]byte{
		0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
		byte(addr >> 8), byte(addr),
	}
}

func (addr XBee16Address) String() string {
	return fmt.Sprintf("16BitAddr:%04X", uint16(addr))
}

type XBee64Address uint64

func (addr XBee64Address) Serialize() *[10]byte {
	result := [10]byte{}
	binary.BigEndian.PutUint64(result[0:8], uint64(addr))
	result[8] = 0xFF
	result[9] = 0xFE
	return &result
}

func (addr XBee64Address) String() string {
	return fmt.Sprintf("64BitAddr:%016X", uint64(addr))
}

type XBeeFullAddress struct {
	NetworkAddress XBee16Address
	MacAddress     XBee64Address
}

func (addr XBeeFullAddress) Serialize() *[10]byte {
	result := [10]byte{}
	binary.BigEndian.PutUint64(result[0:8], uint64(addr.MacAddress))
	binary.BigEndian.PutUint16(result[8:10], uint16(addr.NetworkAddress))
	return &result
}

func (addr XBeeFullAddress) String() string {
	return fmt.Sprintf("FullAddr: { Network: %04X, MAC: %016X } ", uint16(addr.NetworkAddress), uint64(addr.MacAddress))
}

var XBeeCoordinatorAddress = XBee64Address(0)
var XBeeBroadcastAddress = XBee64Address(0xFFFF)
