package frames

import (
	"fmt"
	"testing"
)

func TestDeserializeNIFrame(t *testing.T) {
	rawData := []byte{
		0x00, 0x27, 0x95, 0x00, 0x13, 0xA2, 0x00, 0x12,
		0x34, 0x56, 0x78, 0xFF, 0xFE, 0xC2, 0xFF, 0xFE,
		0x00, 0x13, 0xA2, 0x00, 0x12, 0x34, 0x56, 0x78,
		0x4C, 0x48, 0x37, 0x35, 0x00, 0xFF, 0xFE, 0x01,
		0x01, 0xC1, 0x05, 0x10, 0x1E, 0x00, 0x14, 0x00,
		0x08, 0x0D,
	}
	frame, err := DeserializeFrame(rawData)
	fmt.Println(frame, err)
}
