package frames

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"strings"
)

type NodeIdentificationFrame struct {
	SourceAddress      XBeeAddress
	RemoteAddress      XBeeAddress
	ParentAddress      XBeeAddress
	Options            byte
	NodeIdentifier     string
	NetworkDeviceType  byte
	SourceEvent        byte
	DigiProfileID      uint16
	DigiManufacturerID uint16
	DeviceType         *uint32
	SignalStrength     *byte
}

func (n *NodeIdentificationFrame) deserialize(rawData []byte) error {
	fmt.Printf("data: % X\n", rawData)
	n.SourceAddress = XbeeAddressFromBytes(rawData[8:10], rawData[0:8])
	n.Options = rawData[10]
	n.RemoteAddress = XbeeAddressFromBytes(rawData[11:13], rawData[13:21])
	nullChar := strings.IndexRune(string(rawData[21:]), 0x00)
	n.NodeIdentifier = string(rawData[21 : 21+nullChar])
	rawData = rawData[22+nullChar:]
	fmt.Printf("% X\n", rawData)
	n.ParentAddress = XbeeAddressFromBytes(rawData[0:2], nil)
	n.NetworkDeviceType = rawData[2]
	n.SourceEvent = rawData[3]
	n.DigiProfileID = uint16(rawData[4])<<8 | uint16(rawData[5])
	n.DigiManufacturerID = uint16(rawData[6])<<8 | uint16(rawData[7])
	n.DeviceType = nil
	n.SignalStrength = nil
	if len(rawData) > 11 {
		fmt.Printf("using %X for dtype\n", rawData[8:12])
		dd := binary.BigEndian.Uint32(rawData[8:12])
		n.DeviceType = &dd
	}
	if len(rawData) > 12 {
		ss := rawData[12]
		n.SignalStrength = &ss
	}
	return nil
}

func (n *NodeIdentificationFrame) serialize(buf *bytes.Buffer) []byte {
	panic("Not implemented")
}

func (n *NodeIdentificationFrame) FrameType() FrameType {
	return NODE_IDENTIFICATION_INDICATOR
}

func (n *NodeIdentificationFrame) String() string {
	bld := strings.Builder{}
	bld.WriteString(fmt.Sprintf("NodeIdentificationFrame: { Source: %v, NodeIdent: %q, Remote: %v, Parent: %v, Options: %08b, NetDeviceType: %X, SourceEvent %X",
		n.SourceAddress, n.NodeIdentifier, n.RemoteAddress, n.ParentAddress, n.Options, n.NetworkDeviceType, n.SourceEvent))
	if n.DeviceType != nil {
		bld.WriteString(fmt.Sprintf(", DeviceType: %08X", *(n.DeviceType)))
	}
	if n.SignalStrength != nil {
		bld.WriteString(fmt.Sprintf(", RSSI: %d", *(n.SignalStrength)))
	}
	bld.WriteString(" }")
	return bld.String()
}
