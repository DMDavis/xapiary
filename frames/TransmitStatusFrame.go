package frames

import (
	"bytes"
	"fmt"
)

type ExtendedTransmitStatusFrame struct {
	FrameId            byte
	DestinationAddress XBeeAddress
	RetryCount         byte
	DeliveryStatus     byte
	DiscoveryStatus    byte
}

func (t *ExtendedTransmitStatusFrame) serialize(buffer *bytes.Buffer) []byte {
	panic("implement me")
}

func (t *ExtendedTransmitStatusFrame) deserialize(bytes []byte) error {
	t.FrameId = bytes[0]
	t.DestinationAddress = XbeeAddressFromBytes(bytes[1:3], nil)
	t.RetryCount = bytes[3]
	t.DeliveryStatus = bytes[4]
	t.DiscoveryStatus = bytes[5]
	return nil
}

func (t *ExtendedTransmitStatusFrame) FrameType() FrameType {
	return EXTENDED_TRANSMIT_STATUS
}

func (t *ExtendedTransmitStatusFrame) String() string {
	return fmt.Sprintf("ExtendedTransmitStatusFrame: { Destination: %v, FrameId: %X, Retries: %d, DeliveryStatus: %02X, Discovery: %02X }",
		t.DestinationAddress, t.FrameId, t.RetryCount, t.DeliveryStatus, t.DiscoveryStatus)
}
