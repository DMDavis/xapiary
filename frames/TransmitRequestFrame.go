package frames

import (
	"bytes"
	"errors"
)

type TransmitRequestFrame struct {
	Recipient XBeeAddress
	Payload   []byte
	FrameID   byte
}

func (t *TransmitRequestFrame) serialize(buf *bytes.Buffer) []byte {
	buf.WriteByte(t.FrameID)
	buf.Write(t.Recipient.Serialize()[:])
	buf.WriteByte(0) // broadcast radius (USE AT NH instead)
	buf.WriteByte(0) // transmit bit options (USE TO instead)
	buf.Write(t.Payload)

	return buf.Bytes()
}

func (t *TransmitRequestFrame) deserialize(rawBytes []byte) error {
	if len(rawBytes) < 13 {
		return errors.New("provided frame data is too short to build a tx request frame")
	}
	t.FrameID = rawBytes[0]
	return nil
}

func (t *TransmitRequestFrame) FrameType() FrameType {
	return TX_REQUEST
}
