package frames

import (
	"bytes"
	"fmt"
)

type ReceivePacketFrame struct {
	Sender  XBeeAddress
	Options byte
	Payload []byte
}

func (r *ReceivePacketFrame) serialize(buffer *bytes.Buffer) []byte {
	panic("implement me")
}

func (r *ReceivePacketFrame) deserialize(rawBytes []byte) error {
	r.Sender = XbeeAddressFromBytes(rawBytes[0:2], rawBytes[2:10])
	r.Options = rawBytes[10]
	r.Payload = rawBytes[11:]
	return nil
}

func (r *ReceivePacketFrame) FrameType() FrameType {
	return RECEIVE_PACKET
}

func (r *ReceivePacketFrame) String() string {
	return fmt.Sprintf("ReceivePacketFrame: { Sender: %v, Options: %08b, Payload: [ % X ] %q }", r.Sender, r.Options, r.Payload, string(r.Payload))
}
