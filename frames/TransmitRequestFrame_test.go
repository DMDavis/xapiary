package frames

import (
	"fmt"
	"testing"
)

func TestSerializeFrame(t *testing.T) {
	frame := TransmitRequestFrame{
		FrameID:   0x52,
		Recipient: XBee64Address(0x0013A20012345678),
		Payload:   []byte("TxData"),
	}
	data, err := SerializeFrame(&frame, false)
	if err != nil {
		t.Error(err)
	}
	fmt.Printf("% X\n", data)
}
