package frames

import (
	"fmt"
	"testing"
)

func TestEscaping(t *testing.T) {
	rawSerial := []byte{0x23, 0x11, 0xCB}
	expectedSerial := []byte{0x23, 0x7D, 0x31, 0xCB}

	escapedTestData := escapeBytes(rawSerial)
	if !dataMatches(escapedTestData, expectedSerial) {
		fmt.Println(escapedTestData)
		fmt.Println(expectedSerial)
		t.Fatal("Data does not matched escapement")
	}

	unescapedTestData, err := unescapeBytes(escapedTestData)
	if err != nil {
		t.Fatal("data was not formatted correctly for unescapement")
	}

	if !dataMatches(unescapedTestData, rawSerial) {
		t.Fatal("data was not correctly maintained through escapement")
	}
}

func TestChecksum(t *testing.T) {
	raw := []byte{0x08, 0x01, 0x4E, 0x49, 0x58, 0x42, 0x45, 0x45}
	checkSum := calcChecksum(raw)
	if checkSum != 0x3B {
		t.Fatal("checksum is not calculated correctly")
	}
	zeroSum := calcChecksum(append(raw, checkSum))
	fmt.Println(zeroSum)

	checkSum = calcChecksum([]byte{0x08, 0x01, 0x4E, 0x49, 0x58, 0x42, 0x45, 0x45, 0x3B})
	fmt.Printf("%X", checkSum)
}

func dataMatches(a, b []byte) bool {
	if len(a) != len(b) {
		return false
	}
	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}
