package frames

import (
	"bytes"
	"errors"
	"fmt"
)

type FrameType int

func (ftype FrameType) String() string {
	return apiStringNames[ftype]
}

const (
	INVALID = FrameType(iota)
	LOCAL_AT_REQUEST
	QUEUE_LOCAL_AT_REQUEST
	TX_REQUEST
	EXPLICIT_ADDRESS_TX_REQUEST
	REMOTE_AT_REQUEST
	CREATE_SOURCE_ROUTE_REQUEST
	LOCAL_AT_COMMAND_RESPONSE
	MODEM_STATUS
	EXTENDED_TRANSMIT_STATUS
	RECEIVE_PACKET
	EXPLICIT_RECEIVE_INDICATOR
	IO_SAMPLE_INDICATOR
	SENSOR_READ_INDICATOR
	NODE_IDENTIFICATION_INDICATOR
	REMOTE_AT_COMMAND_RESPONS
	EXTENDED_MODEM_STATUS
	OTA_FIRMWARE_UPDATE_STATUS
	ROUTE_RECORD_INDICATOR
	MTO_ROUTE_REQUEST_INDICATOR
)

var apiStringNames = map[FrameType]string{
	INVALID:                       "INVALID",
	LOCAL_AT_REQUEST:              "AT Command",
	QUEUE_LOCAL_AT_REQUEST:        "AT Command - Queue Parameter Value",
	TX_REQUEST:                    "Zigbee Transmit Request",
	EXPLICIT_ADDRESS_TX_REQUEST:   "Explicit Addressing Zigbee Command Frame",
	REMOTE_AT_REQUEST:             "Remote Command Request",
	CREATE_SOURCE_ROUTE_REQUEST:   "Create Source Route",
	LOCAL_AT_COMMAND_RESPONSE:     "AT Command Response",
	MODEM_STATUS:                  "Modem Status",
	EXTENDED_TRANSMIT_STATUS:      "Zigbee Transmit Status",
	RECEIVE_PACKET:                "Zigbee Receive Packet (AO=0)",
	EXPLICIT_RECEIVE_INDICATOR:    "Zigbee Explicit Rx Indicator (AO=1)",
	IO_SAMPLE_INDICATOR:           "Zigbee I/O Data Sample Rx Indicator",
	SENSOR_READ_INDICATOR:         "XBee Sensor Read Indicator (AO=0)",
	NODE_IDENTIFICATION_INDICATOR: "Node Identification Indicator (AO=0)",
	REMOTE_AT_COMMAND_RESPONS:     "Remote Command Response",
	EXTENDED_MODEM_STATUS:         "Extended Modem Status",
	OTA_FIRMWARE_UPDATE_STATUS:    "Over-the-Air Firmware Update Status",
	ROUTE_RECORD_INDICATOR:        "Route Record Indicator",
	MTO_ROUTE_REQUEST_INDICATOR:   "Many-to-One Route Request Indicator",
}

var frameTypeToAPI_ID = map[FrameType]api_id{
	INVALID:                       invalid,
	LOCAL_AT_REQUEST:              local_at_request,
	QUEUE_LOCAL_AT_REQUEST:        queue_local_at_request,
	TX_REQUEST:                    tx_request,
	EXPLICIT_ADDRESS_TX_REQUEST:   explicit_address_tx_request,
	REMOTE_AT_REQUEST:             remote_at_request,
	CREATE_SOURCE_ROUTE_REQUEST:   create_source_route_request,
	LOCAL_AT_COMMAND_RESPONSE:     local_at_command_response,
	MODEM_STATUS:                  modem_status,
	EXTENDED_TRANSMIT_STATUS:      extended_transmit_status,
	RECEIVE_PACKET:                receive_packet,
	EXPLICIT_RECEIVE_INDICATOR:    explicit_receive_indicator,
	IO_SAMPLE_INDICATOR:           io_sample_indicator,
	SENSOR_READ_INDICATOR:         sensor_read_indicator,
	NODE_IDENTIFICATION_INDICATOR: node_identification_indicator,
	REMOTE_AT_COMMAND_RESPONS:     remote_at_command_respons,
	EXTENDED_MODEM_STATUS:         extended_modem_status,
	OTA_FIRMWARE_UPDATE_STATUS:    ota_firmware_update_status,
	ROUTE_RECORD_INDICATOR:        route_record_indicator,
	MTO_ROUTE_REQUEST_INDICATOR:   mto_route_request_indicator,
}

// frameTypeToApiId returns an api_id based on the input FrameType
// if the FrameType is not a valid value, it will instead return
// the zero-value of api_id, which is incidentally also the
// invalid value.
func frameTypeToApiId(frameType FrameType) api_id {
	return frameTypeToAPI_ID[frameType]
}

type api_id byte

//non obvious fact: the invalid value is the zero value
//that means that any maps will return invalid on map-miss
const (
	invalid                       = api_id(0x00)
	local_at_request              = api_id(0x08)
	queue_local_at_request        = api_id(0x09)
	tx_request                    = api_id(0x10)
	explicit_address_tx_request   = api_id(0x11)
	remote_at_request             = api_id(0x17)
	create_source_route_request   = api_id(0x21)
	local_at_command_response     = api_id(0x88)
	modem_status                  = api_id(0x8A)
	extended_transmit_status      = api_id(0x8B)
	receive_packet                = api_id(0x90)
	explicit_receive_indicator    = api_id(0x91)
	io_sample_indicator           = api_id(0x92)
	sensor_read_indicator         = api_id(0x94)
	node_identification_indicator = api_id(0x95)
	remote_at_command_respons     = api_id(0x97)
	extended_modem_status         = api_id(0x98)
	ota_firmware_update_status    = api_id(0xA0)
	route_record_indicator        = api_id(0xA1)
	mto_route_request_indicator   = api_id(0xA3)
)

var frameIdToFrameInstance = map[api_id]func() Frame{
	receive_packet:                func() Frame { return &ReceivePacketFrame{} },
	node_identification_indicator: func() Frame { return &NodeIdentificationFrame{} },
	extended_transmit_status:      func() Frame { return &ExtendedTransmitStatusFrame{} },
}

func DeserializeFrame(delimeterStrippedData []byte) (Frame, error) {
	length := int(delimeterStrippedData[0])<<8 | int(delimeterStrippedData[1])
	if length != len(delimeterStrippedData)-3 {
		return nil, errors.New("packet is too long")
	}
	apiData := delimeterStrippedData[2:]
	testedChecksum := calcChecksum(apiData)
	if testedChecksum != 0x00 {
		return nil, fmt.Errorf("checksum error %X", testedChecksum)
	}
	frameType := delimeterStrippedData[2]
	if frameInstance, ok := frameIdToFrameInstance[api_id(frameType)]; ok {
		frame := frameInstance()
		err := frame.deserialize(delimeterStrippedData[3 : 2+length])
		return frame, err
	}
	return nil, errors.New("unsupported frame id")
}

func SerializeFrame(frame Frame, escape bool) ([]byte, error) {
	buff := &bytes.Buffer{}
	apiId := frameTypeToApiId(frame.FrameType())
	if apiId == invalid {
		return nil, errors.New("invalid frame specified")
	}
	buff.Write([]byte{0x00, 0x00, 0x00, byte(apiId)})
	internalData := frame.serialize(buff)

	length := len(internalData) - 3 //we prepend space for the length in the buffer creation, so we need to subtract it
	if length >= 0xFFFF {
		return nil, fmt.Errorf("Cannot transmit frames larger than 0xFFFF in size.\nYou tried to send a frame of size %X", length)
	}
	// reserve checksum space
	buff.WriteByte(0)
	finalPacket := buff.Bytes()
	//calc the checksum
	checksum := calcChecksum(finalPacket)
	//then write it into the reserved space
	finalPacket[len(finalPacket)-1] = checksum

	//add length in
	finalPacket[1] = byte(length >> 8)
	finalPacket[2] = byte(length)

	if escape {
		finalPacket = escapeBytes(finalPacket)
	}

	//set the start delimiter
	finalPacket[0] = 0x7E

	return finalPacket, nil
}

type Frame interface {
	serialize(*bytes.Buffer) []byte
	deserialize([]byte) error
	FrameType() FrameType
}

const (
	Delimeter = byte(0x7E)
	Escape    = byte(0x7D)
	Xon       = byte(0x11)
	Xoff      = byte(0x13)
)

var escape_bytes = []byte{
	Delimeter,
	Escape,
	Xon,
	Xoff,
}

func escapeBytes(rawData []byte) []byte {
	buf := bytes.Buffer{}
	for _, v := range rawData {
		for _, escapingByte := range escape_bytes {
			if v == escapingByte {
				buf.WriteByte(Escape)
				v ^= 0x20
				break
			}
		}
		buf.WriteByte(v)
	}
	return buf.Bytes()
}

func unescapeBytes(escapedData []byte) ([]byte, error) {
	incomingData := bytes.NewReader(escapedData)
	buf := bytes.Buffer{}
	for incomingData.Len() > 0 {
		nextByte, _ := incomingData.ReadByte()
		if nextByte == Escape {
			escapedByte, err := incomingData.ReadByte()
			if err != nil {
				return nil, fmt.Errorf("Could not unescape bytes due to: \n%w", err)
			}
			nextByte = escapedByte ^ 0x20
		}
		buf.WriteByte(nextByte)
	}
	return buf.Bytes(), nil
}

func calcChecksum(rawData []byte) byte {
	var checksum byte
	for _, value := range rawData {
		checksum += value
	}
	checksum = 0xFF - checksum
	return checksum
}
