package xapiary

import (
	"bytes"
	"fmt"
	"gitlab.com/DMDavis/xapiary/frames"
	"io"
)

func NewXbee(port io.ReadWriter) *XBee {
	x := &XBee{
		connection:      port,
		incomingPackets: make(chan []byte),
		incomingBytes:   make(chan byte, 64),
		packetRouter:    make(map[frames.FrameType]FrameHandler),
	}

	go x.receiverRoutine()
	go x.packetReader()
	go x.frameParser()
	return x
}

type XBee struct {
	connection      io.ReadWriter
	incomingBytes   chan byte
	incomingPackets chan []byte
	closed          bool

	packetRouter map[frames.FrameType]FrameHandler
}

func (x *XBee) Handle(FrameType frames.FrameType, handler FrameHandler) {
	x.packetRouter[FrameType] = handler
}

func (x *XBee) Transmit(frame frames.Frame) error {
	dataToSend, err := frames.SerializeFrame(frame, true)
	if err != nil {
		return err
	}
	_, err = x.connection.Write(dataToSend)
	return err
}

func (x *XBee) Close() (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("%v", r)
		}
	}()
	x.closed = true
	close(x.incomingBytes)
	close(x.incomingPackets)
	return nil
}

func (x *XBee) packetReader() {
	for packet := range x.incomingPackets {
		frame, err := frames.DeserializeFrame(packet)
		if err != nil {
			fmt.Println(err)
		} else {
			frameType := frame.FrameType()
			if handler, ok := x.packetRouter[frameType]; ok {
				handler(frame)
			} else {
				fmt.Printf("%s packet was not handleable\n", frameType.String())
			}
		}
	}
}

func (x *XBee) receiverRoutine() {
	for !x.closed {
		buffer := make([]byte, 256)
		n, err := x.connection.Read(buffer)
		if err != nil {
			fmt.Println(err)
			continue
		}
		for _, b := range buffer[:n] {
			x.incomingBytes <- b
		}
	}
}

func (x *XBee) frameParser() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("recovered", r)
		}
	}()

	var currentBuffer bytes.Buffer
	var expectedLength int = 0xFFFFFF

	for b := range x.incomingBytes {
		switch b {
		case frames.Delimeter:
			currentBuffer = bytes.Buffer{}
			expectedLength = 0xFFFFFF
			break
		case frames.Escape:
			escapedByte := <-x.incomingBytes
			b = escapedByte ^ 0x20
			fallthrough
		default:
			currentBuffer.WriteByte(b)
			break
		}
		if expectedLength > 0xFFFF && currentBuffer.Len() >= 3 {
			bytes := currentBuffer.Bytes()
			expectedLength = (int(bytes[0]) << 8) | int(bytes[1])
		}
		if currentBuffer.Len() == expectedLength+3 {
			x.incomingPackets <- currentBuffer.Bytes()
			currentBuffer = bytes.Buffer{}
		}
	}
}
